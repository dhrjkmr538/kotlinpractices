package com.kotlinpractices

object HelloKotlin {
    @JvmStatic
    fun main(args: Array<String>) {
        println(hello())
    }

    private fun hello(): String  = "Hello Kotlin Dev!"
}