package com.kotlinpractices

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class HelloKotlinTest {

    @Test
    fun testAdd() {
        assertEquals(42, Integer.sum(19, 23))
    }
}